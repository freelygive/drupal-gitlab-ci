#!/bin/bash

set -e

# Check for an existing MR. If found, exit early.
EXISTING_MR=`curl --silent "${GTILAB_API_BASE}projects/${CI_PROJECT_ID}/merge_requests?state=opened&target_branch=${TARGET_BRANCH}&source_branch=${UPDATE_BRANCH}" --header "PRIVATE-TOKEN:${ACCESS_TOKEN}"`;
if [ "${EXISTING_MR}" == "{}" ]; then
  echo "Merge request already open."
  exit 0
fi

# The description of our new MR, we want to remove the branch after the MR has
# been closed
BODY="{
  \"project_id\": ${CI_PROJECT_ID},
  \"source_branch\": \"${UPDATE_BRANCH}\",
  \"target_branch\": \"${TARGET_BRANCH}\",
  \"remove_source_branch\": true,
  \"force_remove_source_branch\": true,
  \"allow_collaboration\": true,
  \"subscribed\" : true,
  \"title\": \"Update core and modules.\"
}";

curl -X POST "${GTILAB_API_BASE}projects/${CI_PROJECT_ID}/merge_requests" \
  --header "PRIVATE-TOKEN:${ACCESS_TOKEN}" \
  --header "Content-Type: application/json" \
  --data "${BODY}";
