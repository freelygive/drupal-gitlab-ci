#!/bin/bash

set -e

# Ensure we have a full fetch and checkout the right branch, creating if required.
git fetch
git checkout "${UPDATE_BRANCH}" 2>/dev/null || git checkout -b "${UPDATE_BRANCH}"

# Create a settings file to avoid settings.php being overwritten on installation.
SETTINGS_FILE_CONTENTS="<?php
\$databases['default']['default'] = [
  'database' => '$MYSQL_DATABASE',
  'username' => '$MYSQL_USER',
  'password' => '$MYSQL_PASSWORD',
  'host' => '$MYSQL_HOST',
  'port' => '$MYSQL_PORT',
  'driver' => 'mysql',
  'prefix' => '',
];"
echo -e "${SETTINGS_FILE_CONTENTS}" > "${SETTINGS_FILE}"

# Install drush 8 globally.
composer global require drush/drush:8.*

# Basic drupal install so that we have a site to work with.
~/.composer/vendor/bin/drush site-install -y minimal install_configure_form.update_status_module=NULL

# Run the code update.
~/.composer/vendor/bin/drush pm-updatecode -y --check-disabled

# Remove identifying files.
rm ./*.txt

# Check out changes to .gitignore / robots.txt so project modifications are kept.
git checkout .gitignore robots.txt
