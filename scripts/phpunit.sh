#!/usr/bin/env bash

if ! [ -f $CI_PROJECT_DIR/phpunit.xml ] && ! [ -f $CI_PROJECT_DIR/phpunit.xml.dist ]; then
  echo "A phpunit.xml or phpunit.xml.dist is required."
  exit 1
fi

echo "# Starting PHP server"
php -S 0.0.0.0:8888 $DRUPAL_CI_ROUTER > /dev/null 2>&1 &

echo "# Running tests"
../vendor/bin/phpunit -c $CI_PROJECT_DIR --bootstrap core/tests/bootstrap.php
RESULT="$?"

echo "# Moving artifacts into place"
rm $DRUPAL_BUILD_ROOT/web/sites/simpletest/browser_output/.htaccess 2>/dev/null || true
rm $DRUPAL_BUILD_ROOT/web/sites/simpletest/browser_output/*.counter 2>/dev/null || true
mv $DRUPAL_BUILD_ROOT/web/sites/simpletest/browser_output $CI_PROJECT_DIR/test-output 2>/dev/null || true

exit $RESULT
