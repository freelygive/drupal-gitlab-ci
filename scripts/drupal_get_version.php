#!/usr/bin/env php
<?php

if (!isset($argv[1])) {
  print "Missing command line argument: major drupal version (eg 'drupal_get_minor_version.php 9')\n";
  exit(1);
}
if (!isset($argv[2])) {
  print "Missing stability level. Options are 'dev', 'supported', 'supported-dev', 'security'\n";
  print "For debug output, pass 'debug' as this argument.\n";
  exit();
}

$major_version = $argv[1];
$stability = $argv[2];

$output = shell_exec("composer show drupal/core ~{$major_version} -la --format=json");
$versions = json_decode($output)->versions;

$dev = null;
$supported = null;
$supported_dev = null;
$security = null;
$possible_supported_dev = [];
$supported_minor = null;

class Version {
  public function __construct($version) {
    $this->versionString = $version;
    $this->isDev = strpos($version, '.x-dev') !== FALSE;
    $this->isStable = strpos($version, '-') === FALSE;
    $bits = explode('.', $version);
    $this->major = $bits[0];
    $this->minor = $bits[1];
  }

  public function __toString() {
    return $this->versionString;
  }
}

// Versions are already ordered from newest to oldest. 
foreach ($versions as $v) {
  $version = new Version($v);

  if ($version->major === $major_version) {
    if ($version->isDev && !isset($dev)) {
      // The most recent version that ends with .x-dev is the "dev" release.
      $dev = $version;
    }
    elseif ($version->isStable && !isset($supported)) {
      // The most recent version that isn't prerelease is the "supported" release.
      $supported = $version;
    }
    elseif ($version->isStable && isset($supported) && !isset($security)) {
      // The most recent version *before* the supported release 
      // with a different minor version, and  isn't prerelease is the "security" release
      if ($supported->minor > $version->minor) {
        $security = $version;
        // The security release is the oldest we care about, so bail out the loop.
        break;
      }
    }
    
    // We won't know which dev release matches the stable as the dev release will come first
    // so just add all dev releases to a list of possibilities and parse them after we're done.
    if ($version->isDev && isset($dev)) {
      $possible_supported_dev[] = $version;
    }
  }
}

foreach ($possible_supported_dev as $dev_version) {
  if ($dev_version->minor === $supported->minor) {
    $supported_dev = $dev_version;
    break;
  }
}

if (!isset($security)) {
  // No older security releases, so just use supported.
  $security = $supported;
}

$output = [
  'versions' => $versions,
  'dev' => (string)$dev,
  'supported' => (string)$supported,
  'supported-dev' => (string)$supported_dev,
  'security' => (string)$security,
];

if ($stability === 'debug') {
  print_r($output);
}
else {
  print $output[$stability];
}
