#!/usr/bin/env bash

# Get the version of coder from composer.lock.
if [ -f composer.lock ]; then
  CODER_VERSION=$(grep '"name": "drupal/coder"' -A 1 composer.lock | grep 'version' | grep -Eo '[0-9\.]*')
  ACQUIA_STANDARDS_VERSION=$(grep '"name": "acquia/coding-standards"' -A 1 composer.lock | grep 'version' | grep -Eo '[0-9\.]*')
# Otherwise look in composer.json.
else
  CODER_VERSION=$(grep '"drupal/coder": ' composer.json | grep -Eo '[<>=\^~]*[0-9x\.]+')
  ACQUIA_STANDARDS_VERSION=$(grep '"acquia/coding-standards": ' composer.json | grep -Eo '[<>=\^~]*[0-9x\.]+')
fi

# Prefix with a colon for the require command.
if ! [ -z "$CODER_VERSION" ]; then
  echo "Installing Drupal Coder $CODER_VERSION";
  CODER_VERSION=":$CODER_VERSION"
else
  echo "Installing latest Drupal Coder";
fi

# Install Acquia coding standards, if found in composer.
if ! [ -z "$ACQUIA_STANDARDS_VERSION" ]; then
  echo "Installing Acquia Coding Standards $ACQUIA_STANDARDS_VERSION";
  ACQUIA_STANDARDS="acquia/coding-standards:$ACQUIA_STANDARDS_VERSION"
else
  ACQUIA_STANDARDS=""
fi

# Ensure phpcodesniffer composer installer can run plugins.
composer global config --no-plugins allow-plugins.dealerdirect/phpcodesniffer-composer-installer true

# Globally require coder and the phpcs sniffer composer installer.
composer global require "drupal/coder$CODER_VERSION" "$ACQUIA_STANDARDS" dealerdirect/phpcodesniffer-composer-installer --no-progress

# Use a config file if present.
if [ -f phpcs.xml ] || [ -f phpcs.xml.dist ]; then
  ~/.composer/vendor/bin/phpcs
  RESULT="$?"
# Otherwise use the Coder recommended settings.
else
  ~/.composer/vendor/bin/phpcs --standard=$CODER_STANDARD -p --colors --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=node_modules,bower_components,vendor .
  RESULT="$?"
fi

# Exit with the result code.
exit $RESULT
