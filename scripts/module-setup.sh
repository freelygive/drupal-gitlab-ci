#!/bin/sh
set -e

if [ -z "$DRUPAL_VERSION" ]; then 
  # If no explicit DRUPAL_VERSION is specified,
  # then use the DRUPAL_MAJOR_VERSION to calculate it
  echo "# Calculating Drupal $DRUPAL_VERSION_TYPE version for Drupal $DRUPAL_MAJOR_VERSION"
  DRUPAL_VERSION=$(php drupal_get_version.php $DRUPAL_MAJOR_VERSION $DRUPAL_VERSION_TYPE)
fi

echo "# Preparing GIT repo"

# Remove the .git directory from our repo so we can treat it as a path.
cd $CI_PROJECT_DIR
rm .git -rf

# Create our main Drupal project.
echo "# Creating Drupal $DRUPAL_VERSION project"
rm -rf $DRUPAL_BUILD_ROOT
composer create-project drupal/recommended-project:$DRUPAL_VERSION $DRUPAL_BUILD_ROOT --no-install
cd $DRUPAL_BUILD_ROOT

echo "# Setting minimum stability to $DRUPAL_PROJECT_STABILITY"
composer config minimum-stability $DRUPAL_PROJECT_STABILITY

echo "# Allowing all plugins to run"
composer config allow-plugins true

# Set our drupal core version.
echo "# Setting core dependency versions"
composer require drupal/core-composer-scaffold:$DRUPAL_VERSION --no-update
composer require drupal/core-project-message:$DRUPAL_VERSION --no-update
composer require drupal/core-recommended:$DRUPAL_VERSION --no-update
composer require drupal/core-dev:$DRUPAL_VERSION --no-update --dev

# Add our CI repository, which will be prepended.
echo "# Configuring module repo"
composer config repositories.module path $CI_PROJECT_DIR

# Discard changes so we always have the expected resulting code.
composer config discard-changes true

# Enable patching and exit on failure. Patches are only relevant a project
# requires cweagans/composer-patches.
composer config extra.enable-patching true
composer config extra.composer-exit-on-patch-failure true

# Include composer merge plugin so we can include the dev dependencies of the
# project.
composer require wikimedia/composer-merge-plugin --no-update
composer config extra.merge-plugin.include "$CI_PROJECT_DIR/composer.json"
composer config extra.merge-plugin.merge-dev true

echo "# Installing"
composer update --with-all-dependencies --no-progress

# Check if we need to explicitly depend on phpspec/prophecy-phpunit.
# See https://www.drupal.org/node/3176567.
PHPUNIT_VERSION=`composer show phpunit/phpunit | grep 'versions :' | grep -o ' [0-9]' | cut -d ' ' -f 2`
if [ $PHPUNIT_VERSION -ge 9 ]; then
  composer require phpspec/prophecy-phpunit:^2
fi

# Now require our project which will pull itself from the paths.
echo "# Requiring $CI_PROJECT_NAME"
composer require drupal/$CI_PROJECT_NAME:@dev --no-progress

echo "# Updating for merge additions"
composer update --lock
