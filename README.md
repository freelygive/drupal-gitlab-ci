# Drupal GitLab CI
This repository houses includes for `.gitlab-ci.yml` definitions for GitLab's
CI geared towards Drupal projects.

## Drupal Modules
To run coder and PHP Unit via GitLab CI, you need to include the following in 
your project's `.gitlab-ci.yml`:

```yaml
include:
  project: 'freelygive/drupal-gitlab-ci'
  file: 'module.yml'
``` 

This will add the following jobs:
- `coder`: Runs PHPCS with the Drupal Coder ruleset for your module. If you 
  provide a `phpcs.xml`/`phpcs.xml.dist` it will be used. Otherwise the
  `CODER_STANDARD` variable can be set against the `coder` job to override the
  default `Drupal` standard. Note, this job does _not_ perform a full install,
  so only Drupal Coder and Acquia Coding Standards are available.
- `drupal-9-supported`: Runs PHP Unit for your module against the currently 
  supported patch release of Drupal 9.
- `drupal-9-supported-dev`: Runs PHP Unit for your module against dev of the
  currently supported minor branch of Drupal 9.
- `drupal-9-security`: Runs PHP Unit for your module against the current patch 
  release of the security branch of Drupal 9.
- `drupal-9-dev`: Runs PHP Unit for your module against dev of the 
  under-development branch of Drupal 9.
- `drupal-10-dev`: Runs PHP Unit for your module against dev of the 
  under-development branch of Drupal 10.

This also provides an easy way to automate pushing code to the drupal.org
project repository. To enable that, you will need to add the following:

```yaml
drupalorg_branch:
  extends: .drupalorg_branch
drupalorg_tags:
  extends: .drupalorg_tags
```

- `drupalorg_branch`: Will push `*.x-*.x`, `*.x` and `*.*.x` style branches to
  Drupal.org.
- `drupalorg_branch`: Will push tags to Drupal.org.

Both of these will only run when tests have passed and use a 1 minute delay. If
you decide you want to prevent pushing to drupal.org, you can cancel the job 
before it starts.

To use these jobs, you will need to include the following variables in either
your project or group:
- `$DRUPAL_ORG_SSH_KEY`: A private key that has write permissions on the
  drupal.org project repository. This should be a protected variable so it only
  runs on protected branches/tags and you should configure `*.x-*.x` branches 
  and `*` tags to be protected and restrict who can push/merge.
- `$DRUPAL_ORG_HOSTKEYS`: The trusted host keys for git.drupal.org. You can get
  these using `ssh-keyscan git.drupal.org`.

## Dependency stability
If your project has dependencies that do not have a stable release, you can set
the top level `DRUPAL_PROJECT_STABILITY` variable to one of Composer's
stability flags to allow the install. Note that the core dev jobs will set this
to `dev` so that the development branch of core can be installed.

## Using a specific version of these templates
To use a specific version of these templates, you can include with a ref:

```yaml
include:
  project: 'freelygive/drupal-gitlab-ci'
  ref: '1.0.0'
  file: 'module.yml'

variables:
  DRUPAL_GITLAB_CI_REF: '1.0.0'
```

It is important to also include the `DRUPAL_GITLAB_CI_REF` variable so that the
included scripts also pull the correct version. If `ref` and 
`DRUPAL_GITLAB_CI_REF` are not included, HEAD of `master` will be used.
